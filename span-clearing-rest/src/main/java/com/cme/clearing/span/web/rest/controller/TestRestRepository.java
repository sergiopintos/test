package com.cme.clearing.span.web.rest.controller;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "tests", path = "test")
public interface TestRestRepository extends PagingAndSortingRepository<TestEntity, Long> {
}
